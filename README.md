# Sweet Alchemy Jekyll

Sweet Alchemy website re-building with Jekyll.

#### Development Setup

Instal `bundler` and `jekyll` and run bundle install

```
gem install bundler jekyll
bundle install
```

Run jekyll serve to watch changes and run simple webserver

```
jekyll serve
```